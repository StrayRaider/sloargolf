# SloarGolf



## Yapılacaklar

|   Verilme Tarihi   | Planlanan Yazılımsal Geliştirmeler                           | Yapacak Kişi | Önem Derecesi |   Bitiş Durumu   |
| :----------------: | :----------------------------------------------------------- | ------------ | :-----------: | :--------------: |
|      24.06.22      | *Eski kodların incelenip yeni istenilen formata getirilmesi* | **Emre**     |     **8**     |                  |
|      24.06.22      | *Özel güçler eklenmesi*                                      |              |     **7**     |                  |
|      24.06.22      | *Özel güçler panelinin eklenmesi*                            |              |     **5**     |                  |
|      24.06.22      | *Oyun ekranı panelinin geliştirilmesi*                       |              |     **5**     |                  |
|      24.06.22      | *Oyunun sonu ekranı ve credits bölümü*                       | **Emre**     |     **3**     |                  |
|      24.06.22      | *Animasyon eklenmesi işlemleri*                              | **Fatih**    |     **8**     |                  |
|      24.06.22      | *Yazılmış buton sınıfının oyuna eklenmesi*                   | **Emre**     |     **8**     |                  |
|                    |                                                              |              |               |                  |
| **Verilme Tarihi** | **Planlanan Tasarımsal Gelişmeler**                          |              |               | **Bitiş Durumu** |
|      24.06.22      | *Gezegen çizimlerinin yapılması 10 adet*                     | **Semih**    |     **7**     |                  |
|      24.06.22      | *Arkaplan çizimlerinin yapılması 2 adet*                     | **Semih**    |     **5**     |                  |
|      24.06.22      | *Roket çizimi yapılması* 1 adet                              | **Semih**    |     **8**     |                  |
|                    |                                                              |              |               |                  |

